### Setup
I've tested this with Ruby 2.6.3, but I don't see any reason why any recent version wouldn't work.

To reproduce with Capybara

`bundle install`  to install dependencies

`bundle exec rspec example_spec.rb --color` to run the failing test

Or to reproduce with Selenium

 `bundle install`  to install dependencies
 
 `ruby remote_web_driver.rb` to run the failing test
 
### Whats happening?
The VueJs code is using the Vue-Fragment plugin to create a table.  There is some virtual dom manipulation stuff going
on here that I don't fully understand.  In the browser we can see a table with several rows.  The first row has a vue-fragment
where the text is the only content being generated 'Fragment-Text'.  The second row has a vue-fragment where the text is wrapped
in a div "Wrapped-Text".  The Third row with out vue-fragment 'Not-Fragment'.

The first and third row can be detected by capybara while the second can not be found.
