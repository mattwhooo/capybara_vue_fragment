require "selenium-webdriver"


# Defining capabilities of the test session, like browser, OS etc
driver = Selenium::WebDriver.for(:remote, :url => "http://localhost:9515")

begin
	puts "Loading URL"
	driver.navigate.to("file://#{File.expand_path('index.html')}")
	sleep(5)

	puts "Finding text Fragment-Text"
	element = driver.find_element(xpath: "//*[text()='Fragment-Text']")
	element.click

	puts "Finding text Not-Fragment"
	element = driver.find_element(xpath: "//*[text()='Not-Fragment']")
	element.click

	puts "Finding text Wrapped-Text"
	element = driver.find_element(xpath: "//*[text()='Wrapped-Text']").click
	element.click

ensure
	driver.quit
end



