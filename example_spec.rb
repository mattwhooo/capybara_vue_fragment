require 'bundler/setup'
require 'rack/file'
require 'capybara/rspec'

RSpec.configure do |config|
	config.include Capybara::DSL
end

Capybara.default_driver = :selenium_chrome
Capybara.default_max_wait_time = 10
Capybara.app = Rack::File.new File.dirname __FILE__

describe "Example page", :type => :request do

	it "contains an H1 with text 'header'" do
		visit '/index.html'
		expect(page).to have_content('Header')
	end

	it "contains an TD with text 'Fragment-Text'" do
		visit '/index.html'
		expect(page).to have_content('Fragment-Text')
	end

	it "contains an TD with text 'Not-Fragment'" do
		visit '/index.html'
		expect(page).to have_content('Not-Fragment')
	end


	it "contains an TD with text 'Not-Fragment'" do
		visit '/index.html'
		expect(page).to have_content('Wrapped-Text')
	end
end
